package ru.rezonit.www;

import jdk.jfr.Description;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class TestRezonit implements PageObjectSupplier {

    //Открыть браузер на главной странице Rezonit
    @BeforeSuite
    public void openHomePageRezonit() {
        basicMethods().openBrowser(homePage().homeUrl);
    }

    //Закрыть браузер после прохождения кейса
    @AfterSuite
    public void closeHomePageRezonit() {
        basicMethods().closeBrowser();
    }

    @Test
    @Description("Позитивный сценарий: получить цену на заказываемую плату")
    public void getPricePrintedCircuitBoard() {

        //Клик по кнопке "Срочные печатные платы"
        basicMethods().clickElement(homePage().urgentPrintedCircuitBoardsXpath);
        //Клик по полю "Длина"
        basicMethods().clickElement(printedCircuitBoardsPage().lengthFieldXpath);
        //Ввод в поле длина значение 10
        basicMethods().enteringTextInTheField(printedCircuitBoardsPage().lengthFieldXpath, "10");
        //Клик по полю "Ширина"
        basicMethods().clickElement(printedCircuitBoardsPage().widthFieldXpath);
        //Ввод в поле шири на значение 20
        basicMethods().enteringTextInTheField(printedCircuitBoardsPage().widthFieldXpath, "20");
        //Клик по полю "Количество штук"
        basicMethods().clickElement(printedCircuitBoardsPage().numberOfPiecesXpath);
        //Ввод в поле количество штук значение 20
        basicMethods().enteringTextInTheField(printedCircuitBoardsPage().numberOfPiecesXpath, "20");
        //Нажать кнопку "Рассчитать"
        basicMethods().clickElement(printedCircuitBoardsPage().calculateButtonXpath);
        //Ожидание отображения элемента "Предварительная стоимость"
        basicMethods().waitingForElement(printedCircuitBoardsPage().preliminaryCostXpath);
        //Проверить отображение предварительной цены
        basicMethods().checkedDisplayingElement(printedCircuitBoardsPage().preliminaryCostXpath);
    }

    @Test
    @Description("Негативный сценарий: превышение значения ширины платы (999)")
    public void getPricePrintedCircuitBoardNegative() {

        //Клик по кнопке "Срочные печатные платы"
        basicMethods().clickElement(homePage().urgentPrintedCircuitBoardsXpath);
        //Клик по полю "Длина"
        basicMethods().clickElement(printedCircuitBoardsPage().lengthFieldXpath);
        //Ввод в поле длина значение 10
        basicMethods().enteringTextInTheField(printedCircuitBoardsPage().lengthFieldXpath, "10");
        //Клик по полю "Ширина"
        basicMethods().clickElement(printedCircuitBoardsPage().widthFieldXpath);
        //Ввод в поле шири на значение 999
        basicMethods().enteringTextInTheField(printedCircuitBoardsPage().widthFieldXpath, "999");
        //Проверка отображение уведомления об неверных размерах платы
        basicMethods().checkedDisplayingElement(printedCircuitBoardsPage().alertDangerXpath);
    }
}
