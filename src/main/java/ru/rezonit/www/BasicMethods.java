package ru.rezonit.www;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Condition.appear;

public class BasicMethods {

    //Открыть браузер
    public void openBrowser(String url) {
        Selenide.open(url);
    }

    //Метод для закрытия окна браузера
    public void closeBrowser() {
        Selenide.closeWindow();
    }

    //Метод для нажатия на элементы
    public void clickElement(SelenideElement element) {
        element.click();
    }

    //Метод для ввода данных
    public void enteringTextInTheField(SelenideElement element, String addText) {
        element.sendKeys(addText);
    }

    //метод для проверки отображение элемента на экране
    public void checkedDisplayingElement(SelenideElement checkedElement) {
        checkedElement.should(appear);
    }

    //Метод для ожидания отображения элемента
    public void waitingForElement(SelenideElement waitingElement) {
        waitingElement.should(appear, Duration.ofSeconds(15));
    }
}