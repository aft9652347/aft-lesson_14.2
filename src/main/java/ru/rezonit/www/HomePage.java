package ru.rezonit.www;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class HomePage {

    //Базовая страница
    public String homeUrl = "https://www.rezonit.ru/";

    //Кнопка "Срочные печатный платы"
    public SelenideElement urgentPrintedCircuitBoardsXpath = $x(
            "//ul[@class = 'd-none d-lg-flex justify-content-between main-menu']/li/a[@href='/pcb/']"
    );

}
