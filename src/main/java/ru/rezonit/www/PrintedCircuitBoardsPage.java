package ru.rezonit.www;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class PrintedCircuitBoardsPage {
    //Поле "Длина"
    public SelenideElement lengthFieldXpath = $x("//input[@id='calculator-input-1']");
    //Поле "Ширина"
    public SelenideElement widthFieldXpath = $x("//input[@id='calculator-input-2']");
    //Поле "Количество штук"
    public SelenideElement numberOfPiecesXpath = $x("//input[@id='calculator-input-3']");
    //Кнопка "Рассчитать"
    public SelenideElement calculateButtonXpath = $x("//button[@id='calculate']");
    //Предварительная стоимость
    public SelenideElement preliminaryCostXpath = $x("//span[@id='total-price']");
    //Уведомление об ошибке
    public SelenideElement alertDangerXpath = $x("//div[@class='alert alert-danger']");
}