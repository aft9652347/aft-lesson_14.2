package ru.rezonit.www;

public interface PageObjectSupplier {

    //Методы для создания классов страниц
    default BasicMethods basicMethods() {
        return new BasicMethods();
    }

    default HomePage homePage() {
        return new HomePage();
    }

    default PrintedCircuitBoardsPage printedCircuitBoardsPage() {
        return new PrintedCircuitBoardsPage();
    }
}
